<?php require_once "./code.php"; ?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>S02: Repetition Control Structures and Array Manipulation</title>
</head>
<body>

	<h1>Divisibles of Five</h1>

	<?php forLoop(); ?>

	<br>

	<h1>Array Manipulation</h1>

	<?php array_push($student, 'John Smith') ?>

	<pre><?php print_r($student); ?></pre>

	<pre><?php echo count($student); ?></pre>

	<?php array_push($student, 'Jane Smith') ?>

	<pre><?php print_r($student); ?></pre>

	<pre><?php echo count($student); ?></pre>

	<?php array_shift($student); ?>
	
	<pre><?php print_r($student); ?></pre>

	<pre><?php echo count($student); ?></pre>




</body>
</html>
